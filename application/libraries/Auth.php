<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * AUTH class
 */

class Auth {

	public $CI;	
	protected $current_user_id;	
	protected $current_user;
	

	/**
	 * Constructor
	 */ 	 
	public function __construct(){
	
		$this->CI =& get_instance();
		$this->CI->load->model('User_model', '', TRUE);
		
		// load session library
        $this->CI->load->library('session');

		$user_data = $this->CI->User_model->getUserInfo($this->current_user_id);
		
		if($this->isLogged() && isset($user_data[0]) ){
			$this->current_user_id 	= $this->CI->session->userdata('user_id');		
			$this->current_user  	= $this->CI->User_model->getUserInfo($this->current_user_id)[0];
		}
	
	}
	
	public function Login($username,$password){		
		$u_id=$this->CI->User_model->getUserIdByUsernamePassword($username,$password);

		
		
		
		if($u_id===false){
			return -2;
		}
		else{

			$user_data = $this->CI->User_model->getUserInfo($u_id);

			if($user_data[0]['activated'] ){
				// check sessions
				$this->CI->session->set_userdata('user_logged', true);
				$this->CI->session->set_userdata('user_id', $u_id);
				$this->CI->session->set_userdata('user_level', $user_data[0]['user_level']);
								
				// set user
				$this->current_user_id=$this->CI->session->userdata('user_id');		

                if( permisions( $user_data[0]['user_level'], array( USER_LEVEL_SUPERADMIN ) ) ) {
                
                    if( session_status() == PHP_SESSION_NONE ) {
                        session_start();
                    }
                }
				return $u_id;
			}
			else{
				return -1;
			}
		}		
	}
	
	
	public function isLogged(){
		return $this->CI->session->userdata('user_logged')===true;
	}
	
	public function logOut(){
		$this->CI->session->unset_userdata('user_logged');
		$this->CI->session->unset_userdata('user_id');
		$this->CI->session->unset_userdata('cart');
	}
	
	public function getUserId(){
		if($this->isLogged())
			return $this->current_user_id;
		else
			return false;
	}
	
	public function getUser(){
		if($this->isLogged())
			return $this->current_user;
		else
			return false;
	}

	// returns user ID by username & pass, FALSE if not found
	function getUserIdByUsernamePassword($username, $password)
	{
		$password = md5(PW_SALT . $password);

		$query = "SELECT st_user_acc.id FROM st_user_acc  WHERE st_user_acc.email=? AND st_user_acc.password=?";
		$result = $this->db->query($query, array($username, $password));
		if ($result) {
			if ($result->num_rows() > 0) {
				$row = $result->row_array();
				return $row['id'];
			} else {
				return false;
			}
		}
	}
	
	
}
