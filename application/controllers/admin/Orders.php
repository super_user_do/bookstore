<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends MY_Controller {

	var $user_permissions = array (USER_LEVEL_SUPERADMIN);
	var $modul  =	'categores';
	 
	function __construct(){	
		parent::__construct();
		$this->load->library('session');
		$this->load->model('cart_model');
		$this->load->model('books_model');
		$this->load->model('user_model');
		$this->load->model('orders_model');
		$this->load->helper('url_helper');
		$this->load->helper('form');
		$this->load->helper('url');
	}

	public function index(){
		$this->data['orders']   = $this->getNotActiveOrders();
		$this->load->view('admin/admin_orders_list', $this->data);	
	}

	public function active(){
		$this->data['orders']   = $this->getNotActiveOrders();
		$this->data['sended']   = false;
		$this->load->view('admin/admin_orders_list', $this->data);	
	}

	public function sended(){
		$this->data['orders']   = $this->getNotActiveOrders(false);
		$this->data['sended']   = true;
		$this->load->view('admin/admin_orders_list', $this->data);	
	}

    public function getNotActiveOrders( $active = true ){
		if ($active) {
			$nonActiveOrders = $this->orders_model->getActiveOrders();
		}else{
			$nonActiveOrders = $this->orders_model->getNonActiveOrders();
		}
        // echo '<pre>'. print_r($nonActiveOrders, true) .'</pre>'; exit; 

        $nonActiveOrders_array = array();
        foreach ($nonActiveOrders as $order){
            //Вземане на информацията за потребителя
            $productAddedBy  = $this->getUserInfo($order['createdBy']);
            $productInfo     = $this->getProduct($order['productId']);
            $nonActiveOrders_array[] = array(
                                    'id'            => $order['id'],
                                    'userInfo'      => $productAddedBy,
                                    'productInfo'   => $productInfo,
                                    'quantity'      => $order['quantity'],
                                    'totalPrice'    => $order['totalPrice']
                                    );
        }

		return $nonActiveOrders_array;
    }

	public function getUserInfo($id)
	{
		return $this->orders_model->getUser($id);
	}

	public function getProduct($id)
	{
		return $this->orders_model->getProduct($id);
	}

	public function completeOrder()
	{
		$orderId = $_POST['orderId'];
		$bookId = $_POST['itemId'];
		$quantity = $_POST['quantity'];
		$nonActiveOrders = $this->orders_model->completeOrder($bookId, $quantity, $orderId);
		redirect('admin/orders/sended');
		exit();
	}

	
}

