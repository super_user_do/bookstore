<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	var $user_permissions = array(USER_LEVEL_FREE,USER_LEVEL_REGISTERED,USER_LEVEL_SUPERADMIN);
	 
	function __construct(){	
		parent::__construct();	
	}

	public function index(){
		if($this->auth->isLogged()){
			redirect('admin/home/page');
		}else{
			redirect('admin/user/login');
		}
	}

	public function page(){
		$this->load->view('admin/admin_home');
	}
	

	
}

