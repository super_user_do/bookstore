<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller {

	var $user_permissions = array(USER_LEVEL_FREE,USER_LEVEL_REGISTERED,USER_LEVEL_SUPERADMIN);
	var $modul  =	'user';
	 
	function __construct(){	
		parent::__construct();	
	}

	public function index(){
		redirect('admin/user/login');
	}
	
	public function login(){		
		$this->load->view('admin/login',$this->data);				
	}
	
	public function doLogin(){
		$username=$_POST['username'];
		$password=$_POST['password'];
		$u_id=$this->auth->Login($username,$password);	
		

		if($u_id <= 0){
			$this->session->set_flashdata('user_errors', 'Incorect username and/or password !');
			redirect('admin/user/login');
		}
		else{
			redirect('admin/home');
		}		
		
	}
	
	public function doLogout(){
		$this->auth->logOut();
		$this->load->view('admin/login',$this->data);
	}

	public function list(){
		$this->data['users'] = $this->User_model->getUsers();
		$this->load->view('admin/admin_users_list', $this->data);	
	}

	
}

