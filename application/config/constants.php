<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/* AV CONSTANTS */

define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

define('USER_LEVEL_SUPERADMIN', 199);
define('USER_LEVEL_REGISTERED', 109);
define('USER_LEVEL_FREE', 110);

define('PAGE_TYPE_FRONT', 201);
define('PAGE_TYPE_ADMIN', 202);


define('OFFER_UNPUBLISHED', 			0 ); //unpublished 
define('OFFER_PUBLISHED', 				1 ); //published in site and callcenter 
define('OFFER_PUBLISHED_CALLCENTER', 	2 ); //published in callcenter only

define('PW_SALT', 'SOL_ZA_PCSTORE');

/* End of file constants.php */
/* Location: ./application/config/constants.php */

