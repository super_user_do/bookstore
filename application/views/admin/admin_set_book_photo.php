<?php include 'admin_head.php' ?>

<?php echo form_open_multipart('books/setBookPhoto/' . $id . '', array('class' => 'dropzone', 'id' => "dropZoneImage", 'method' => "POST")); ?>

<div class="fallback">
	<input type="file" name="bookPhotoFile" multiple />
</div>


<div id="message" class="dz-message" data-dz-message><span>
		<span style="font-size:200px; width: 200;
                    display: block;
                    margin-left: auto;
                    margin-right: auto; " class="material-icons">cloud_upload</span>
		<img data-dz-thumbnail /> </img>
</div>



<?php echo form_close(); ?>



<button type="submit" id="button" style="display: block; margin-left: auto; margin-right: auto; margin-top: 15px;" class="btn btn-primary"><?= lang('submit') ?></button>



<?php include 'admin_footer.php' ?>

<script>
	Dropzone.options.dropZoneImage = {
		url: this.location,
		paramName: "bookFile", //the parameter name containing the uploaded file
		maxFilesize: 2, //in mb
		uploadMultiple: false,
		maxFiles: 1, // allowing any more than this will stress a basic php/mysql stack
		addRemoveLinks: false,
		acceptedFiles: '.png,.jpg,.gif', //allowed filetypes
		autoProcessQueue: false,

		init: function() {

			var myDropzone = this;

			$("#button").click(function(e) {
				e.preventDefault();
				myDropzone.processQueue();
			});

			this.on('sending', function(file, xhr, formData) {
				// Append all form inputs to the formData Dropzone will POST
				var data = $('#dropZone').serializeArray();
				$.each(data, function(key, el) {
					formData.append(el.name, el.value);
				});

			});
			this.on("success", function(file, responseText) {
				swal({
					title: "<?= lang('save_success') ?>",
					type: "success"
				}).then(function() {
					window.location.href = '/admin/products/';
				});
			});
			this.on("addedfile", function(file) {
				// swal({title: "Успешно added", type: "success"})
			});
		}
	};
</script>
