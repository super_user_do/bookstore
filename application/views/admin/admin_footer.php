



<script>
/**
 * Custom validator for letters (uppercase/lowercase) 
 * numbers 0-9
 * white space and "."
 */
$.validator.addMethod("lettersAndNumbers", function(value, element) {
    return this.optional(element) || /^[a-zA-Z0-9 .]+$/i.test(value);
});
/**
 * Custom validator for numbers 0-9
 * white space and "."
 */
$.validator.addMethod("onlyNumbers", function (value, element) {
    return this.optional(element) || /^[08{789}\d{7} +]+$/i.test(value);
});

/**
 * Custom validator for only letters  that are cyrilic
 */
// $.validator.addMethod("onlyBulgarianLetters", function (value, element) {
//     return this.optional(element) || /^[а-яА-Я-]+$/i.test(value);
// });

/**
 * Custom validator for only letters  that are cyrilic
 */
// $.validator.addMethod("bulgarianLettersAndSymbols", function (value, element) {
//     return this.optional(element) || /^[а-яА-Я0-9_.,: "']+$/i.test(value);
// });


/**
* Is it valid mail
*/
$.validator.addMethod("isMailValid", function (value, element) {
    return this.optional(element) || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value); 
});


/**
 * Custom validator for contains at least one lower-case letter
 */
$.validator.addMethod("atLeastOneLowercaseLetter", function (value, element) {
    return this.optional(element) || /[a-z]+/.test(value);
}, "Must have at least one lowercase letter");
 
/**
 * Custom validator for contains at least one upper-case letter.
 */
$.validator.addMethod("atLeastOneUppercaseLetter", function (value, element) {
    return this.optional(element) || /[A-Z]+/.test(value);
}, "Must have at least one uppercase letter");
 
/**
 * Custom validator for contains at least one number.
 */
$.validator.addMethod("atLeastOneNumber", function (value, element) {
    return this.optional(element) || /[0-9]+/.test(value);
}, "Must have at least one number");
 
/**
 * Custom validator for contains at least one symbol.
 */
$.validator.addMethod("atLeastOneSymbol", function (value, element) {
    return this.optional(element) || /[!@#$%^&*()]+/.test(value);
}, "Must have at least one symbol");


$(document).ready(function(){
    $("#createBookForm").validate({
    rules: {
        title: {
        required: true,
        minlength: 5,
        maxlength: 50,
        bulgarianLettersAndSymbols: true,
        },
        author: {
            required: true,
            minlength: 5,
            maxlength: 50,
            bulgarianLettersAndSymbols: true,
        },
        description: {
            required: true,
            maxlength: 1500,
            bulgarianLettersAndSymbols: false,
        },
        price: {
            onlyNumbers:true,
            required: true,
        },
        quantity: {
            onlyNumbers:true,
            required: true,
        }
    },
    messages: {
        title: {
        required: "Field is required",
        minlength: "Must contain at least 5 characters",
        maxlength: "Must not contain more than 50 characters",
        bulgarianLettersAndSymbols: "Only letters and digits",
        },
        author: {
            required: "Field is required",
            minlength: "Must contain at least 5 characters",
            maxlength: "Must contain no more than 5 characters",
            bulgarianLettersAndSymbols: "Only letters and digits",
            },
        description: {
            required: "Field is required",
            minlength: "Must contain at least 5 characters",
            maxlength: "Must contain no more than 1500 characters",
            bulgarianLettersAndSymbols: "Only letters and digits",
        },
        price: {
            onlyNumbers: "Only numbers",
            required: "Field is required",
        },
        quantity: {
            onlyNumbers: "Only numbers",
            required: "Field is required",
        }
    }
    })
});




</script>

</body>
</html>