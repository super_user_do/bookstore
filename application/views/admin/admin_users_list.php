<?php include 'admin_head.php' ?>

<table>

<th>
	<tr>
		<td>Name</td>
		<td>Surname</td>
		<td>Phone number</td>
		<td>Email</td>
		<td>Active</td>
	</tr>
</th>

<?php foreach($users as $user) :?>
	<th>
		<tr>
			<td><?= $user['username'] ?></td>
			<td><?= $user['usrFamName'] ?></td>
			<td><?= $user['phoneNumber'] ?> number</td>
			<td><?= $user['email'] ?></td>
			<td><?= $user['activated'] ? 'Yes' : 'No' ?></td>
		</tr>
	</th>
<?php endforeach ?>

</table>

<?php include 'admin_footer.php' ?>
