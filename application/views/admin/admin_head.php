<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Admin Panel - Home</title>

		<!-- DROPZONE JS -->
		<script src="/assets/libraries/dropzone.js"></script>
	<style type="text/css" href="/assets/libraries/dropzone.css"></style>
	<style type="text/css" href="/assets/libraries/basic.min.css"></style>

	<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">

	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>


	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>



	<!-- SWEET ALERT -->
	<script src="https://unpkg.com/sweetalert2@7.8.2/dist/sweetalert2.all.js"></script>
	<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=w369bt4tmx2htffidw9yug68ld18m6ywrqrpravublp6qqon"></script>

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<link rel="stylesheet" href="/assets/admin/css/style.css">
</head>
<body>




	<div class="side_bar"> 
		<ul>
			<li><a href="/admin">Home</a></li>
			<li><a href="/admin/user/list">Users </a></li>
			<li><a href="/admin/categories">Categories</a></li>
			<li><a href="/admin/products"> Products</a></li>
			<li><a href="/admin/orders/active"> Waiting orders </a></li>
			<li><a href="/admin/orders/sended"> Finished orders</a></li>
			<li><a href="/admin/user/doLogOut"> Logout/Exit admin</a> </li>
		</ul>
	</div>

