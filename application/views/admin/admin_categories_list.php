<?php include 'admin_head.php' ?>

<table>

	<th>
		<tr>
			<td>Number</td>
			<td>Name</td>
		</tr>
	</th>

	<?php foreach ($categories as $category) : ?>
		<th>
			<tr>
				<td><?= $category['id'] ?> </td>
				<td><?= $category['title'] ?> </td>
			</tr>
		</th>
	<?php endforeach ?>

</table>


<div class="container">
<div class="inner_wrapper inner_wrapper--sm">
	<form action="/admin/categories/createCategory" method=POST ?>

		<div class="row">
			<div class="col-md-12">
				<label for="prio"><?= lang('prio');?> </label>
				<input type="number" class="field" name="Prio" />
			</div>
			<div class="col-md-12">
				<label for="title"><?= lang('title') ?></label>
				<input type="text" class="field" name="Title" />
			</div>
			<div class="col-md-12">
				<label for="Keyword"><?= lang('keyword') ?></label>
				<input type="text" class="field" name="Keyword" />
			</div>
		</div>



		<!-- <div class="col-sm-12 col-sm-offset-4">   <input type="text" name="Title"/> </div>
        <div class="col-sm-12 col-sm-offset-4">  <label for="keyword">Keyword</label> <input type="text" name="Keyword"/> </div> -->

		<div class="row" style="margin:25px 0px 0px 100px">
			<div class="col-sm-12 col-sm-offset-4">
				<input type="submit" name="submit" value="<?= lang('create_new_cat') ?>" />
			</div>
		</div>
	</form>
</div><!-- /.inner_wrapper -->
	<div class="field__errors__normal">
		<?php echo form_open('/admin/categories/createCategory'); ?>
		<?php echo validation_errors(); ?>
	</div>
</div>
<?php include 'admin_footer.php' ?>
