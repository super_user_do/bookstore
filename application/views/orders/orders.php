<?php $this->load->view('templates/header');  ?>

<table>
    <thead>
        <tr>
            <td class="name">   <?=lang('number')?></td>
            <td class="posts">  <?=lang('name')?></td>
            <td class="snaps">  <?=lang('famName')?></td>
            <td class="ranking"><?=lang('email')?></td>
            <td class="acorns"> <?=lang('phone')?></td>
            <td class="ranking"><?=lang('adress')?></td>
            <td class="ranking"><?=lang('description')?></td>
            <td class="ranking"><?=lang('title')?></td>
            <td class="ranking"><?=lang('quantity')?></td>
            <td class="ranking"><?=lang('totalPrice')?></td>
            <td class="ranking"><?=lang('acceptOrder')?></td>
        </tr>
    <tbody>     
	<?php foreach($orders as $key => $order): ?>

    

        <tr>
            <form action="/orders/completeOrder" method="POST" ?>
                <td class="name"><?=++$key?></td>
                <td class="posts">  <input type="text" name="Name" value="<?=$order['userInfo']['Name']?>">                                         </td>
                <td class="snaps">  <input type="text" name="FamilyName" value="<?=$order['userInfo']['FamilyName']?>">                             </td>
                <td class="acorns"> <input type="text" name="Mail" value="<?=$order['userInfo']['Mail']?>">                                         </td>
                <td class="ranking"><input type="text" name="PhoneNumber" value="<?=$order['userInfo']['PhoneNumber']?>">                           </td>
                
                <td class="ranking"><input type="text" name="adress" value="<?=$order['userInfo']['Address']?>">                                    </td>
                <td class="ranking"><?=$order['userInfo']['Comment']?>                                                                              </td>
                <td class="ranking"><?=$order['productInfo']['title']?>                                                                             </td>
                <td class="ranking"><input type="text" name="quantity" value="<?=$order['quantity']?>">                                             </td>
                <td class="ranking"><input type="text" name="totalPrice" value="<?=$order['totalPrice']?>">                                         </td>
                <td class="ranking" style="display:none"><input type="text" name="itemId" value="<?=$order['productInfo']['id']?>">                 </td>
                <td class="ranking" style="display:none"><input type="text" name="orderId" value="<?=$order['id']?>">                 </td>
                <td class="ranking">
                <button type="submit" class="btn btn-success"><?=lang('acceptOrder')?></button></td>
            </form>


              
   
	
	<?php endforeach ?>

        </tr>
    </table>


<?php $this->load->view('templates/footer');  ?>
