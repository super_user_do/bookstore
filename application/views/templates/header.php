<?php
$this->load->library('session');
?>
<html>
	<head>
	<title>Bookstore</title>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
  
	<!-- DROPZONE JS -->
	<script src="/assets/libraries/dropzone.js"></script>
	<style type="text/css" href="/assets/libraries/dropzone.css"></style>
	<style type="text/css" href="/assets/libraries/basic.min.css"></style>

	<link rel="stylesheet" href="/assets/libraries/dropzone.css">

	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


	<!-- SWEET ALERT -->
	<script src="https://unpkg.com/sweetalert2@7.8.2/dist/sweetalert2.all.js"></script>
	<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=w369bt4tmx2htffidw9yug68ld18m6ywrqrpravublp6qqon"></script>


	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet"  href="/assets/libraries/style.css">

<script>
/**
 * Custom validator for letters (uppercase/lowercase) 
 * numbers 0-9
 * white space and "."
 */
$.validator.addMethod("lettersAndNumbers", function(value, element) {
    return this.optional(element) || /^[a-zA-Z0-9 .]+$/i.test(value);
});
/**
 * Custom validator for numbers 0-9
 * white space and "."
 */
$.validator.addMethod("onlyNumbers", function (value, element) {
    return this.optional(element) || /^[08{789}\d{7} +]+$/i.test(value);
});

/**
 * Custom validator for only letters  that are cyrilic
 */
// $.validator.addMethod("onlyBulgarianLetters", function (value, element) {
//     return this.optional(element) || /^[а-яА-Я-]+$/i.test(value);
// });

/**
 * Custom validator for only letters  that are cyrilic
 */
// $.validator.addMethod("bulgarianLettersAndSymbols", function (value, element) {
//     return this.optional(element) || /^[а-яА-Я0-9_.,: "']+$/i.test(value);
// });


/**
* Is it valid mail
*/
$.validator.addMethod("isMailValid", function (value, element) {
    return this.optional(element) || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value); 
});


/**
 * Custom validator for contains at least one lower-case letter
 */
$.validator.addMethod("atLeastOneLowercaseLetter", function (value, element) {
    return this.optional(element) || /[a-z]+/.test(value);
}, "Must have at least one lowercase letter");
 
/**
 * Custom validator for contains at least one upper-case letter.
 */
$.validator.addMethod("atLeastOneUppercaseLetter", function (value, element) {
    return this.optional(element) || /[A-Z]+/.test(value);
}, "Must have at least one uppercase letter");
 
/**
 * Custom validator for contains at least one number.
 */
$.validator.addMethod("atLeastOneNumber", function (value, element) {
    return this.optional(element) || /[0-9]+/.test(value);
}, "Must have at least one number");
 
/**
 * Custom validator for contains at least one symbol.
 */
$.validator.addMethod("atLeastOneSymbol", function (value, element) {
    return this.optional(element) || /[!@#$%^&*()]+/.test(value);
}, "Must have at least one symbol");


$(document).ready(function(){
    $("#createBookForm").validate({
    rules: {
        title: {
        required: true,
        minlength: 5,
        maxlength: 50,
        // bulgarianLettersAndSymbols: false,
        },
        author: {
            required: true,
            minlength: 5,
            maxlength: 50,
            // bulgarianLettersAndSymbols: false,
        },
        description: {
            required: true,
            maxlength: 1500,
            // bulgarianLettersAndSymbols: false,
        },
        price: {
            onlyNumbers:true,
            required: true,
        },
        quantity: {
            onlyNumbers:true,
            required: true,
        }
    },
    messages: {
        title: {
        required: "<?php echo lang('required');?>",
        minlength: "Трябва да съдържа поне 5 символа",
        maxlength: "Не трябва да съдържа повече от  50 символа",
        bulgarianLettersAndSymbols: "Само букви и цифри",
        },
        author: {
            required: "<?php echo lang('required');?>",
            minlength: "Трябва да съдържа поне 5 символа",
            maxlength: "Не трябва да съдържа поне 5 символа",
            bulgarianLettersAndSymbols: "Само букви и цифри",
            },
        description: {
            required: "<?php echo lang('required');?>",
            minlength: "Трябва да съдържа поне 5 символа",
            maxlength: "Не трябва да повече от  1500 символа",
            bulgarianLettersAndSymbols: "Само букви и цифри",
        },
        price: {
            onlyNumbers: "Само Цифри",
            required: "<?php echo lang('required');?>",
        },
        quantity: {
            onlyNumbers: "Само Цифри",
            required: "<?php echo lang('required');?>",
        }
    }
    })
});


/**
* Използва се създаване на потребител без регистрация
*
*/





$(document).ready(function(){
    $("#addNewAdress").validate({
    rules: {
        address:{
            required: true,
            minlength: 3,
            maxlength: 50,
            bulgarianLettersAndSymbols: false,
        },
        messages: {
            address: { 
                required : "<?php echo lang('required');?>",
                bulgarianLettersAndSymbols: "<?php echo lang('only_cyrilic_alpha');?>",
                minlength: "<?php echo lang('at_least_5_symbols');?>`",
                maxlength: "<?php echo lang('max_50_symbols');?>",
                },
            }
        }
    })
});




$(document).ready(function(){
    $("#changeProfileInfo").validate({
        
        // За hidden input
        // ignore: ".ignore",

    rules: {
        username:{
            required: true,
            minlength: 3,
            maxlength: 50,
        },
        userFamName:{
            required: true,
            minlength: 3,
            maxlength: 50,
        },
        phoneNumber: {
            minlength: 10,
            maxlength: 13,
            onlyNumbers:true,
            required: true,
        },
        Email: {
            required: true,
            isMailValid : true,
        },
    }, 
    messages: {
            username:{
                required:  "<?php echo lang('required');?>",
                minlength: "<?php echo lang('at_least_3_symbols');?>",
                maxlength: "<?php echo lang('max_50_symbols');?>",
            },
            userFamName:{
                required:  "<?php echo lang('required');?>",
                minlength: "<?php echo lang('at_least_3_symbols');?>",
                maxlength: "<?php echo lang('max_50_symbols');?>",
            },
            phoneNumber: {
                minlength: "<?php echo lang('not_valid_number');?>",
                maxlength: "<?php echo lang('not_valid_number');?>",
                onlyNumbers: "<?php echo lang('only_numbers');?>",
                required: "<?php echo lang('required');?>",
            },
            Email: {
                required : "<?php echo lang('required');?>",
                isMailValid : "<?php echo lang('enter_valid_email');?>",
            }
    }    

    })
});


// var validator = $("#signupform").validate({
//     rules: {
//         firstname: "required",
//         lastname: "required",
//         username: {
//             required: true,
//             minlength: 2,
//             remote: "users.php"
//         }
//     },
//     messages: {
//         firstname: "Enter your firstname",
//         lastname: "Enter your lastname",
//         username: {
//             required: "Enter a username",
//             minlength: jQuery.format("Enter at least {0} characters"),
//             remote: jQuery.format("{0} is already in use")
//         }
//     }
// });

</script>

       <!-- <script src="https://www.google.com/recaptcha/api.js" async defer></script> -->



  </head>
        <body>
        
        <?php include 'navigation.php'; ?>