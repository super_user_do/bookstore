﻿<?php $this->load->view('templates/header');  ?>

<?php 

$attributes = array('role'=>"form", 'id'=>"createTempRegister");
echo form_open('register/orderName',$attributes); ?>


<div class="container">
  <div class="form-row">
    <div class="form-group">
      <label for="name"><?php echo lang("name");?></label>
      <input type="text" class="form-control" name="name" id="name" placeholder="<?php echo lang('name');?>">
    </div>


    <div class="form-group">
      <label for="familyName"><?php echo lang("famName");?></label>
      <input type="text" class="form-control" name="familyName" id="familyName" placeholder="<?php echo lang('famName');?>">
    </div>


    <div class="form-group">
      <label for="Email">Email</label>
      <input type="text" class="form-control" name="txtEmail" id="txtEmail" placeholder="Email">
    </div>


    <div class="form-group">
      <label for="number"><?php echo lang("number");?></label>
      <!-- <div class="input-group"> -->
        <!-- <span class="input-group-addon" id="basic-addon1">+359</span> -->
        <input value="+359" type="text" class="form-control" name="number"  id="number">
      <!-- </div> -->
    </div>


    <div class="form-group">
      <label for="adress"><?php echo lang("adress");?></label>
      <input type="text" class="form-control" name="adress" id="adress" placeholder="<?php echo lang("adress");?>">
    </div>


    <div class="form-group col-md-6">
      <label for="city"><?php echo lang("city");?></label>
        <select name="chooseCity" id="chooseCity" class="form-control">
          <option value="choose" disabled selected><?php echo lang("choose_city");?></option>
          <option value="Sofia"><?php echo lang("city_sofia");?></option>
          <option value="Burgas"><?php echo lang("city_bourgas");?></option>
          <option value="Varna"><?php echo lang("city_varna");?></option>
          <option value="StaraZagora"><?php echo lang("city_stara_zagora");?></option>
        </select>
    </div>
    
    <div name="chooseCity" id="chooseCity" class="form-group col-md-6">
      <label for="Office"><?php echo lang("office");?></label>
      <select name="selectOffice" id="selectOffice" class="form-control" disabled="disabled">

        </select>
    </div>


    <div class="form-group">
      <label for="comment"><?php echo lang('comment_on_the_order');?></label>
      <input type="text" class="form-control" name="comment" id="comment" placeholder="<?php echo lang('comment_on_the_order');?>">
    </div>


    <div class="form-group col-md-6">
        <input type="submit" id="saveCart" class="btn btn-success center-block" value="<?php echo lang('complete_the_order');?>">
    </div>


    <div class="form-group col-md-6">
      <input type="button" id="emptyCart" class="btn btn-danger center-block" value="<?php echo lang('cartEmpty');?>">
    </div>
  </div>
  <?php echo form_close(); ?>

<script type="text/javascript">    
  $('#emptyCart').click(function() {
      $.ajax({
          url: '/BuyItemsInCart/clearCard',
          method: 'POST',
          // data: data
      })
      .done(function(status) {
          if (status == "deleted"){
            // alert(data.message);
              window.location.href = "/books/page/33/1/2/title/asc";
          }
          else{
            window.location.href = "/books/page/33/1/2/title/asc";
          }
        })
  })

  

$('#chooseCity').change(function(e){
  // if($('#createTempRegister').valid() == true){
  $('#selectOffice').attr("value",'').text('');
  var choosedCity = $(this).val();
    e.preventDefault()
      $.ajax({
        url: '/BuyItemsInCart/chooseShop/'+choosedCity,
        method: 'POST',
        dataType: 'json',
        data: choosedCity,
      }).done(function(data){
            if(typeof data.shopArr != 'undefined')
            {
              $.each(data.shopArr,function(key,value)
              {
                $('#selectOffice').prop("disabled", false);
                $('#selectOffice')
                .append($("<option></option>")
                    .attr("value",value)
                    .text(value)); 
              });
            }
            else
            {
              alert(typeof data.shopArr);
              alert('nope');
            }
      })
// }
// else{ return false;}
})


/**
Запазване на информацията от формата и предаването на даните
*/

  // if($('#createTempRegister').valid() == true){
    $('#saveCart').click(function(e){
      e.preventDefault();
      var data_form = $('#createTempRegister').serialize();
        $.ajax({
          url: '/BuyItemsInCart/saveInformation/',
          method : 'POST',
          dataType: 'json',
          data: data_form,
        }).done(function(data){
          /**
          * Проверява дали формата е попълнена правилно
          * и тогава продължава с изпълнението на поръчката
          */
          if($('#createTempRegister').valid() == true){
              if(data.success == true){
                alert('поръчката завършена');
                window.location.href = "/books/page/33/1/2/title/asc";
              }else if(data.error == "<?php echo lang('cartEmpty');?>" ){
                alert(data.error);
                window.location.href = "/books/page/33/1/2/title/asc";
              }
              else{
                alert(data.error);
              }
          }
          
        }).fail(function() {    
                    return false;
                });

    })
// }else{
  // return false;
// }



$(document).ready(function(){
    $("#createTempRegister").validate({

    rules: {
        name: {
            required: true,
            minlength: 3,
            maxlength: 50,
            onlyBulgarianLetters: false,
        },
        familyName: {
            required: true,
            minlength: 4,
            maxlength: 50,
            onlyBulgarianLetters: false,
        },
        txtEmail: {
            required :true,
            isMailValid : true,
        },
        number: {
            minlength: 10,
            maxlength: 13,
            onlyNumbers:true,
            required: true,
        },
        comment: {
            maxlength: 150,
            bulgarianLettersAndSymbols: false,
        },
        chooseCity:{
            required: true,
        },
        adress:{
            required: true,
            minlength: 3,
            maxlength: 50,
            bulgarianLettersAndSymbols: false,
        }
    },
    messages: {
            name: {
                required: "<?php echo lang('required');?>",
                minlength: "<?php echo lang('must_contain_at_least_9_symbols');?>",
                maxlength: "<?php echo lang('must_contain_at_least_9_symbols');?>",
                onlyBulgarianLetters: "Само символи на кирлица",
            },
            familyName: {
                required: "<?php echo lang('required');?>",
                minlength: "<?php echo lang('at_least_4');?>",
                maxlength: "<?php echo lang('not_50');?>",
                onlyBulgarianLetters: "<?php echo lang('only_cyrilic_alpha');?>",
            },
            txtEmail: {
                required: "<?php echo lang('required');?>",
                isMailValid: "<?php echo lang('not_valid_email');?>",
            },
            number: {
                minlength: '<?php echo lang("not_valid_number");?>',
                maxlength: '<?php echo lang("not_valid_number");?>',
                onlyNumbers: '<?php echo lang("only_numbers");?>',
                required: "<?php echo lang('required');?>",
            },
            comment: {
                // required: "<?php echo lang('required');?>",
                // minlength: "Трябва да съдържа поне 5 символа",
                maxlength: "<?php echo lang('not_more_than_250_symbols');?>",
                bulgarianLettersAndSymbols: "<?php echo lang('only_cyrilic_alpha');?>",
            },
            chooseCity:{
                required: "<?php echo lang('required');?>",
            },
            adress:{
                required:  "<?php echo lang('required');?>",
                minlength: "<?php echo lang('at_least_3_symbols');?>",
                maxlength: "<?php echo lang('max_50_symbols');?>",
                bulgarianLettersAndSymbols: "<?php echo lang('only_cyrilic_alpha');?>",
            },
        }
    })
    

});


</script>

<?php $this->load->view('templates/footer');  ?>
