<?php $this->load->view('templates/header');  ?>


<div class="container">
<?php
$attributes = array('role'=>"form", 'id'=>"registerUser", 'autocomplete'=>"off");
echo form_open('user/registerUser', $attributes); ?>

    
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="username"><?php echo lang("name");?></label>
            <input type="text" class="form-control" id="username" name="username" placeholder="<?php echo lang("please_enter");?> <?php echo lang("name");?>">
        </div>
        
        <div class="form-group col-md-6">
            <label for="userFamName"><?php echo lang("famName");?></label>
            <input type="text" class="form-control" id="userFamName" name="userFamName" placeholder="<?php echo lang("please_enter");?> <?php echo lang("famName");?>">
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="registerPassword"><?php echo lang("password");?></label>
            <input type="password" class="form-control" id="registerPassword" name="registerPassword" placeholder="<?php echo lang("password");?>">
        </div>

        <div class="form-group col-md-6">
            <label for="passwordCheck"><?php echo lang("please_confirm");?> <?php echo lang("password");?></label>
            <input type="password" class="form-control" id="passwordCheck" name="passwordCheck" placeholder="<?php echo lang("please_confirm");?> <?php echo lang("password");?>">
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="phoneNumber"><?php echo lang("phone");?></label>
            <input type="text" class="form-control" id="phoneNumber" name="phoneNumber" placeholder="<?php echo lang("phone");?>">
        </div>

        <div class="form-group col-md-6">
            <label for="Email">Email</label>
            <input type="text" class="form-control" id="Email" name="Email" placeholder="Email">
            <div id="mailError"></div>
        </div>
    </div>
    
    <!-- <div class="form-row">
            <div  name="CaptchaCode" id="capatcha">
            <div class="g-recaptcha" data-callback="recaptchaCallback" style=" display: inline-block;" data-sitekey="6LcmqFgUAAAAABs4CXeDUOVtpHbUSz8Uj8DDKz8o"></div>
            <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">

        </div>
    </div> -->
    
    <div class="form-row" style="padding-top:25px;">
        <input type="submit" id="registerUserSubmit" class="btn btn-success center-block" value="<?php echo lang("register_yourself");?>">
    </div> 

<?php echo form_close(); ?>

</div>

	<script>

		// function recaptchaCallback() {
		// $('#hiddenRecaptcha-error').hide();
		// };  
		$(document).ready(function(){
			$('#Email').on('change', function(e){
				e.preventDefault()

				var email = $(this).val();
				// alert(mail);
				// alert(array);
				$.ajax({
					url: '/user/checkMail/' + email,
					method: 'POST',          
					dataType: 'json',
					data: email,
				}).done(function(data) {
					if(data.success == false){
						$("#Email").val('<?php echo lang("email_is_not_free");?>');
					}
				})
			})

		
			$('#registerUserSubmit').on('click', function(e) {
				e.preventDefault()
				if($('#registerUser').valid() == true){
				
				var data_form = $('#registerUser').serialize();
				
				$.ajax({
					url: '/user/registerNewUser',
					method: 'POST',          
					dataType: 'json',
					data: data_form,
				})
					.done(function(data) {
						if(data.success == true){
							alert('<?php echo lang("successful_registration");?>');
							window.location.href = "/user/login";
						}  
					}).fail(function() {
						alert('<?php echo lang("error_msg");?>');
					});
				}
			});

			
		
			$("#registerUser").validate({
				
				// За hidden input
				// ignore: ".ignore",

				rules: {
					username:{
						required: true,
						minlength: 3,
						maxlength: 50,
					},
					userFamName:{
						required: true,
						minlength: 3,
						maxlength: 50,
					},
					registerPassword:{
						required: true,
						atLeastOneLowercaseLetter: true,
						atLeastOneUppercaseLetter: true,
						atLeastOneNumber: true,
						atLeastOneSymbol: true,
						minlength: 8,
						maxlength: 40,
					},
					passwordCheck: {
						required: true,
						equalTo: registerPassword,
					},
					phoneNumber: {
						minlength: 10,
						maxlength: 13,
						onlyNumbers:true,
						required: true,
					},
					Email: {
						required: true,
						isMailValid : true,
					},
					addrArea: {
						required: true,
						minlength: 5,
						maxlength: 50,
						bulgarianLettersAndSymbols: false,
					},
					addrCity: {
						required: true,
						minlength: 5,
						maxlength: 50,
						bulgarianLettersAndSymbols: false,
					},
					addrNeibr: {
						required: true,
						minlength: 5,
						maxlength: 50,
						bulgarianLettersAndSymbols: false,
					},
					adress: {
						required: true,
						minlength: 5,
						maxlength: 50,
						bulgarianLettersAndSymbols: false,
					},
					hiddenRecaptcha: {
							required: function () {
								if (grecaptcha.getResponse() == '') {
									return true;
								} else {
									return false;
								}
							}
					}
				}, 
				messages: {
						username:{
							required:  "<?php echo lang('required');?>",
							minlength: "Трябва да съдържа поне 3 символа",
							maxlength: "<?php echo lang('max_50_symbols');?>",
						},
						userFamName:{
							required:  "<?php echo lang('required');?>",
							minlength: "Трябва да съдържа поне 3 символа",
							maxlength: "<?php echo lang('max_50_symbols');?>",
						},
						registerPassword: {
							required: "<?php echo lang('required');?>",
							atLeastOneUppercaseLetter: "<?php echo lang('must_contain_atleast_1_upper_letter');?>",
							atLeastOneLowercaseLetter: "<?php echo lang('must_contain_atleast_1_smaller_letter');?>",
							atLeastOneNumber: "<?php echo lang("must_contain_atleast_1_digit");?>" ,
							atLeastOneSymbol: "Трябва да съдържа поне 1 специален символ",
							minlength: "<?php echo lang('Трябва да съдържа поне 8 символа');?>" ,
							maxlength: "<?php echo lang('not_more_than_50');?>",
							
						},
						passwordCheck:{ 
							// passWordValidationUpper: "Поне един главен символ",
							required: "<?php echo lang('required');?>",
							equalTo: 'паролите не съвпадат',
						},
						phoneNumber: {
							minlength: 'Номерът не е валиден',
							maxlength: 'Номеръ не е валиден',
							onlyNumbers: "Само Цифри",
							required: "<?php echo lang('required');?>",
						},
						Email: {
							required : "<?php echo lang('required');?>",
							isMailValid : "Моля въведете валиден мейл адрес",
						},
						adress: {
							required : "<?php echo lang('required');?>",
							bulgarianLettersAndSymbols: "Само символи на кирлица",
							minlength: "Трябва да съдържа поне 5 символа",
							maxlength: "<?php echo lang('max_50_symbols');?>",
						},
						addrArea: { 
							required : "<?php echo lang('required');?>",
							bulgarianLettersAndSymbols: "Само символи на кирлица",
							minlength: "Трябва да съдържа поне 5 символа",
							maxlength: "<?php echo lang('max_50_symbols');?>",
						},
						addrCity: { 
							required : "<?php echo lang('required');?>",
							bulgarianLettersAndSymbols: "Само символи на кирлица",
							minlength: "Трябва да съдържа поне 5 символа",
							maxlength: "<?php echo lang('max_50_symbols');?>",
						},
						addrNeibr: { 
							required : "<?php echo lang('required');?>",
							bulgarianLettersAndSymbols: "Само символи на кирлица",
							minlength: "Трябва да съдържа поне 5 символа",
							maxlength: "<?php echo lang('max_50_symbols');?>",
						},
						hiddenRecaptcha: {
							required: "Рекаптча кодът е задължителен",
							remote: "Рекаптча кодът трябва да се попълни отново",
						}
				}    

				});
			});
		});
	</script>






<?php $this->load->view('templates/footer');  ?>
