<?php $this->load->view('templates/header');  ?>

<?php                    
$attributes = array('role'        =>    "form",
                    'action'      =>    "javascript:;",
                    'method'      =>    "post",
                    'id'          =>    "changeProfileInfo",
                    'class'       =>    "form-horizontal");

echo form_open('user/info', $attributes); ?>


    <div class="form-group">
        <label class="col-lg-3 control-label"><?=lang('name')?></label>
            <div class="col-lg-7">
                <input class="form-control" type="text" name="username"  placeholder="<?=$userinfo['username']?>">
            </div>
    </div>



    <div class="form-group">
        <label class="col-lg-3 control-label"><?=lang('famName')?></label>
            <div class="col-lg-7">
                <input class="form-control" type="text" name="userFamName"  placeholder="<?=$userinfo['usrFamName']?>">
        </div>
    </div>


    <div class="form-group">
        <label class="col-lg-3 control-label"><?=lang('phone')?>:</label>
            <div class="col-lg-7">
                <input class="form-control" type="text" name="phoneNumber"  placeholder="<?=$userinfo['phoneNumber']?>">
        </div>
    </div>
   
    <div class="form-group">
        <label class="col-lg-3 control-label"><?=lang('email')?>:</label>
            <div class="col-lg-7">
                <input class="form-control" type="text" id="Email" name="Email" placeholder="<?=$userinfo['email']?>">
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-md-3 control-label"></label>
                <div class="col-md-7">
                    <input type="button" id="changeInfo" class="btn btn-primary" value="<?php echo lang('save_changes');?>">
                    <span></span>
        </div>
    </div>


<?=form_close()?>


<?php $this->load->view('templates/footer');  ?>


<script>

$('#Email').on('change', function(e){
        e.preventDefault()
        var email = $(this).val();
        $.ajax({
            url: '/user/checkMail/' + email,
            method: 'POST',          
            dataType: 'json',
            data: email,
        }).done(function(data) {
            if(data.success == false){
                swal({title: "<?php echo lang('email_is_not_free');?>", type: "error"});
                $("#Email").val("<?php echo lang('email_is_not_free');?>");
                $("#Email").addClass( "form-control error" );
            }
        })
    });

$('#changeInfo').on('click', function(e) {
    e.preventDefault()
    var email = $(this).val();
    setTimeout(function() {

    if($('#changeProfileInfo').valid() == true){
    var info = $('#changeProfileInfo').serializeArray();    
        $.ajax({
            url: '/user/changeInfo',
            method: 'POST',          
            dataType: 'json',
            data: {info},
        })
            .done(function(data) {
                if(data.success == true){
                    swal({title: "<?php echo lang('you_have_successfully_updated_your_data');?>", type: "success"});
                    location.reload(true);
                }else{
                        alert(data.error);
                }
        
                
            }).fail(function() {
                alert("<?php echo lang('error_msg');?>");
            });
        }
    },200);});

</script>