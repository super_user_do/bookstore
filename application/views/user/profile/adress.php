<?php $this->load->view('templates/header');?>


<!-- <section id="login"> -->
    <div class="container">
   
    	   
        	    <div class="form-wrap">
					<p style="font-size:25px;"><?php echo lang("hello");?> <?=$currUserName?></p>
					<p style="text-align:center;"> <?php echo lang("your_addresses");?>: </p>
					<hr>

					


								<?php foreach( $adress as $adress_item ) : ?>

									<div class="address_overview">
										<div class="table-responsive">

										
											<table class="table table-bordered">
												<thead>
													<tr>
														<th>   
															<?php echo lang("area");?>
														</th>
														<th>
															<?php echo lang("city");?>
														</th>
														<th>
															<?php echo lang("Neighborhood");?>
														</th>
														<th>
															<?php echo lang("adress");?>
														</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															<div>
																<?=$adress_item['AddrArea']?>
															</div>
														</td>
														<td>
															<div>
																<?=$adress_item['AddrCity']?>
															</div>
														</td>
														<td>
															<div>
																<?=$adress_item['AddrNeibr']?>
															</div>
														</td>
														<td>
															<div>
																<?=$adress_item['Addr']?>
															</div>
														</td>										
													</tr>
												</tbody>
											</table>

										</div>

										<?php if ( $adress_item['is_active'] == 1 ) : ?>
									

											<ul class="list_actions">
												<li>
													<button type="button" class="btn btn-default">
														<span class="glyphicon glyphicon-ok-sign" style="color:green; font-size:25px" aria-hidden="true"></span>
													</button>
												</li>

												<li>
													<button type="button" class="btn btn-default editAdress" data-toggle="modal" data-target="#myModal" data-id="<?=$adress_item['id']?>">
														<span class="glyphicon glyphicon-edit" style="color:green; font-size:25px" aria-hidden="true"></span>
													</button>
												</li>
											</ul>
														
										<?php else : ?>

											<ul class="list_actions">
												<li>
													<button type="button" class="btn btn-default setAsACtive" data-id="<?=$adress_item['id']?>">
														<span class="glyphicon glyphicon-open " style="color:blue; font-size:25px" aria-hidden="true"></span>
													</button>
												</li>
												
												<li>
													<button type="button" class="btn btn-default editAdress" data-toggle="modal" data-target="#myModal" data-id="<?=$adress_item['id']?>">
														<span class="glyphicon glyphicon-edit" style="color:green; font-size:25px" aria-hidden="true"></span>
													</button>
												</li>
	
												<li>
													<button type="button" class="btn btn-default deleteAdress" data-id="<?=$adress_item['id']?>">
														<span class="glyphicon glyphicon-minus" style="color:red; font-size:25px" aria-hidden="true"></span>
													</button>
												</li>
											
											</ul>
																					
										<?php endif; ?>

									</div><!-- /.address_overview -->
								
								<?php endforeach; ?>
						
								<?php 
								
									echo ' <div class="page_actions page_actions--spacing">
												<button type="button" class="btn btn-default editAdress" data-toggle="modal" data-target="#addAdressModal" ">
													'.lang("add_address").' <span class="glyphicon glyphicon-plus" style="color:green; font-size:11px" aria-hidden="true"></span>
												</button>
											</div>
									';
								
								?>

        	    </div>
   
    </div> <!-- /.container -->
<!-- </section> -->


<!-- Modal for edit adress -->
<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a class="close" data-dismiss="modal" style="display: inline-block;">×</a>
                    <p style="text-align:center; font-size:20px;">
						<?php echo lang("change_address");?>
					</p>
            </div>

            <div class="modal-body" style="margin-bottom:20%">
                <form role="form" id ="changeAdress" action="user/adress">

                    <div class="form-row">   
                        <div class="form-group col-md-6">
                            <input type="text" class="form-control" id="addrArea" name="addrArea" placeholder="<?php echo lang('area');?>">
                        </div>

                        <div class="form-group col-md-6">
                            <input type="text" class="form-control" id="addrCity" name="addrCity" placeholder="<?php echo lang('city');?>">
                        </div>

                        <div class="form-group col-md-6">
                            <input type="text" class="form-control" id="addrNeibr" name="addrNeibr" placeholder="<?php echo lang('Neighborhood');?>">
                        </div>

                        <div class="form-group col-md-6">
                            <input type="text" class="form-control" id="adress" name="adress" placeholder="<?php echo lang('adress');?>">
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                    <input type="submit" id="changeAdressSubmit" class="btn btn-success" value="<?php echo lang('change_text');?>">
                    <a href="#" class="btn btn-danger" data-dismiss="modal">X</a>
            </div>
        </div>
    </div>
</div>


<!-- Modal for edit adress -->
<div id="addAdressModal" class="modal fade" role="dialog">
<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a class="close" data-dismiss="modal" style="display: inline-block;">×</a>
                    <p style="text-align:center; font-size:20px;">
						<?php echo lang("add_new_address");?>
					</p>
            </div>

            <div class="modal-body" style="margin-bottom:20%">
                <form role="form" id ="addAdress" action="/user/addAdres">

                    <div class="form-row">   
                        <div class="form-group col-md-6">
                            <input type="text" class="form-control" id="addAddrArea" name="addAddrArea" placeholder="<?php echo lang('area');?>">
                        </div>

                        <div class="form-group col-md-6">
                            <input type="text" class="form-control" id="addAddrCity" name="addAddrCity" placeholder="<?php echo lang('city');?>">
                        </div>

                        <div class="form-group col-md-6">
                            <input type="text" class="form-control" id="addAddrNeibr" name="addAddrNeibr" placeholder="<?php echo lang('Neighborhood');?>">
                        </div>

                        <div class="form-group col-md-6">
                            <input type="text" class="form-control" id="addNewAdres" name="addNewAdres" placeholder="<?php echo lang('adress');?>">
                        </div>
                    </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" id="addAdressSubmit" class="btn btn-success" value="<?php echo lang('just_add');?>">
                            <a href="#" class="btn btn-danger" data-dismiss="modal">X</a>
                    </div>
                </form>
        </div>
    </div>
</div>

<?php $this->load->view('templates/footer'); ?>


<script>
$(document).ready(function(){
	$('#addAdressSubmit').on('click', function (e) {
		e.preventDefault();

			var data = {};
			var newArea = $('#addAddrArea').val();
			var newCity = $('#addAddrCity').val();
			var newNeibr = $('#addAddrNeibr').val();
			var newAadress = $('#addNewAdres').val();
			// console.log(info);

			// data:{username: username, password:password},
			data.newArea   = newArea;
			data.newCity   = newCity;
			data.newNeibr  = newNeibr;
			data.newAadress = newAadress;
				
		if($('#addAdress').valid() == true){
			$.ajax({
					url: '/user/addAdres',
					method: 'POST',          
					dataType: 'json',
					data: data,
				})
				
			.done(function(data){
				if(data.success == true){
					swal({title: "<?=lang('save_success');?>", type: "success"}).then(function(){ 
						location.reload();
						});
					}
				else{
					swal({title: "<?php echo lang('unsuccessfully_saved');?>", type: "fail"});
					}
				})
		}
	});

	$('.deleteAdress').on('click', function(e) {
		e.preventDefault()
		var address_id = $(this).attr('data-id');
		// console.log(address_id);
			$.ajax({
				url: '/user/deleteAdress/' + address_id,
				method: 'POST',          
				dataType: 'json',
				data: address_id,
			})
				.done(function(data) {
					if(data.success == true){
						
						swal({title: "<?php echo lang('successfully_deleted');?>", type: "success"}).then(function(){ 
								location.reload();
								}
						);


					}
				})
	});




	$('.setAsACtive').on('click', function(e) {
		e.preventDefault()
		var address_id = $(this).attr('data-id');
			$.ajax({
				url: '/user/setActiveAdress/' + address_id,
				method: 'POST',          
				dataType: 'json',
				data: address_id,
			})
				.done(function(data) {
					// alert(data);
					if(data.success == true){
						// swal("Запазено като активен", "" ,"success");
						swal({title: "<?php echo lang('saved_as_active');?>", type: "success"}).then(function(){ 
								location.reload();
								}
						);
					}
				})
	});



	$('#myModal').on('click', function (e) {
		var data = {};
		var id = $('.editAdress').data('id');
		var area = $('#addrArea').val();
		var city = $('#addrCity').val();
		var neibr = $('#addrNeibr').val();
		var adress = $('#adress').val();
		// console.log(info);
		data.id     =  id;
		data.area   = area;
		data.city   = city;
		data.neibr  = neibr;
		data.adress = adress;
			if($('#changeAdress').valid() == true){
		
				$.ajax({
						url: '/user/changeAdress/',
						method: 'POST',          
						dataType: 'json',
						data: data,
					})
					
					.done(function(data){
						if(data.success == true){
							location.reload(true);
						}
					})

		}
	});





    $("#changeAdress").validate({
    	rules: {
            adress: {
                required: true,
                minlength: 3,
                maxlength: 50,
                bulgarianLettersAndSymbols: false,
            },
            addrArea:{
                required: true,
                minlength: 3,
                maxlength: 50,
                bulgarianLettersAndSymbols: false,
            },
            addrCity:{
                required: true,
                minlength: 3,
                maxlength: 50,
                bulgarianLettersAndSymbols: false,
            },
            addrNeibr:{
                required: true,
                minlength: 3,
                maxlength: 50,
                bulgarianLettersAndSymbols: false,
            }
        },
        messages: {
                adress: { 
                    required : "<?php echo lang('required');?>",
                    bulgarianLettersAndSymbols: "<?php echo lang('only_cyrilic_alpha');?>",
                    minlength: "<?php echo lang('at_least_5_symbols');?>",
                    maxlength: "<?php echo lang('max_50_symbols');?>",
                    },
                addrArea: {
                    required : "<?php echo lang('required');?>",
                    bulgarianLettersAndSymbols: "<?php echo lang('only_cyrilic_alpha');?>",
                    minlength: "<?php echo lang('at_least_5_symbols');?>",
                    maxlength: "<?php echo lang('max_50_symbols');?>",
                },
                addrCity:{
                    required : "<?php echo lang('required');?>",
                    bulgarianLettersAndSymbols: "<?php echo lang('only_cyrilic_alpha');?>",
                    minlength: "<?php echo lang('at_least_5_symbols');?>",
                    maxlength: "<?php echo lang('max_50_symbols');?>",
                },
                addrNeibr:{
                    required : "<?php echo lang('required');?>",
                    bulgarianLettersAndSymbols: "<?php echo lang('only_cyrilic_alpha');?>",
                    minlength: "<?php echo lang('at_least_5_symbols');?>",
                    maxlength: "<?php echo lang('max_50_symbols');?>",
                },
            }
    });


	
    $("#addAdress").validate({
    	rules: {
    	        addNewAdres: {
                required: true,
                minlength: 3,
                maxlength: 50,
                bulgarianLettersAndSymbols: false,
            },
            addAddrArea:{
                required: true,
                minlength: 3,
                maxlength: 50,
                bulgarianLettersAndSymbols: false,
            },
            addAddrCity:{
                required: true,
                minlength: 3,
                maxlength: 50,
                bulgarianLettersAndSymbols: false,
            },
            addAddrNeibr:{
                required: true,
                minlength: 3,
                maxlength: 50,
                bulgarianLettersAndSymbols: false,
            }
        },
    		messages: {
            addNewAdres: { 
                required : "<?php echo lang('required');?>",
                bulgarianLettersAndSymbols: "<?php echo lang('only_cyrilic_alpha');?>",
                minlength: "<?php echo lang('at_least_5_symbols');?>",
                maxlength: "<?php echo lang('max_50_symbols');?>",
                },
            addAddrArea: {
                required : "<?php echo lang('required');?>",
                bulgarianLettersAndSymbols: "<?php echo lang('only_cyrilic_alpha');?>",
                minlength: "<?php echo lang('at_least_5_symbols');?>",
                maxlength: "<?php echo lang('max_50_symbols');?>",
            },
            addAddrCity:{
                required : "<?php echo lang('required');?>",
                bulgarianLettersAndSymbols: "<?php echo lang('only_cyrilic_alpha');?>",
                minlength: "<?php echo lang('at_least_5_symbols');?>",
                maxlength: "<?php echo lang('max_50_symbols');?>",
            },
            addAddrNeibr:{
                required : "<?php echo lang('required');?>",
                bulgarianLettersAndSymbols: "<?php echo lang('only_cyrilic_alpha');?>",
                minlength: "<?php echo lang('at_least_5_symbols');?>",
                maxlength: "<?php echo lang('max_50_symbols');?>",
            },
        }
    });






});


</script>
