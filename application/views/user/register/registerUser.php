<?php $this->load->view('templates/header');  ?>

<!-- register__form -->

<div class="container">
<?php
$attributes = array('role'=>"form", 
                    'id'=>"registerUser",
                     'autocomplete'=>"off");
                     
echo form_open('user/registerUser', $attributes); ?>

    
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="username"><?=lang('name')?></label>
            <input type="text" class="form-control" id="username" name="username" placeholder="<?=lang('name')?>">
        </div>
        
        <div class="form-group col-md-6">
            <label for="userFamName"><?=lang('famName')?></label>
            <input type="text" class="form-control" id="userFamName" name="userFamName" placeholder="<?=lang('famName')?>">
        </div>
    </div> 

    <div class="form-row" >
        <div class="form-group col-md-6">
            <label for="registerPassword"><?=lang('password')?></label>
            <input type="password" class="form-control" id="registerPassword" name="registerPassword" placeholder="<?=lang('password')?>">
        </div>

        <div class="form-group col-md-6">
            <label for="passwordCheck"><?=lang('confrimPassword')?></label>
            <input type="password" class="form-control" id="passwordCheck" name="passwordCheck" placeholder="<?=lang('confrimPassword')?>">
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-3">
            <label for="addrArea"><?=lang('area')?></label>
            <input type="text" class="form-control" id="addrArea" name="addrArea" placeholder="<?=lang('area')?>">
        </div>

        <div class="form-group col-md-3">
            <label for="addrCity"><?=lang('city')?></label>
            <input type="text" class="form-control" id="addrCity" name="addrCity" placeholder="<?=lang('city')?>">
        </div>

        <div class="form-group col-md-3">
            <label for="addrNeibr"><?=lang('Neighborhood')?></label>
            <input type="text" class="form-control" id="addrNeibr" name="addrNeibr" placeholder="<?=lang('Neighborhood')?>">
        </div>

        <div class="form-group col-md-3">
            <label for="addr"><?=lang('adress')?></label>
            <input type="text" class="form-control" id="adress" name="adress" placeholder="<?=lang('adress')?>">
        </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="phoneNumber"><?=lang('phone')?></label>
            <input type="text" class="form-control" id="phoneNumber" name="phoneNumber" placeholder="<?=lang('phone')?>">
        </div>

        <div class="form-group col-md-6">
            <label for="Email"><?=lang('email')?></label>
            <input type="text" class="form-control" id="Email" name="Email" placeholder="<?=lang('email')?>">
            <div id="mailError"></div>
        </div>
    </div>
    
	<?php /* 
    <div class="form-row">
            <div  name="CaptchaCode" id="capatcha">
            <div class="g-recaptcha" data-callback="recaptchaCallback" style=" display: inline-block;" data-sitekey="6LdyTxMbAAAAAOIU85BvUpfWELa5ouRC7ui2F1Rn"></div>
            <label id="hiddenRecaptcha-error" class="error" for="hiddenRecaptcha"><?=lang('required')?></label>
            <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">

        </div>
    </div>
	*/?>
    
    <div class="form-row" style="padding-top:25px;">
        <input type="submit" id="registerUserSubmit" class="btn btn-success center-block" value="<?=lang('register')?>">
    </div> 

<?php echo form_close(); ?>

</div>

<script type="text/javascript">

// function  recaptchaCallback(){
    //   alert("callback working");
    //   $('#hiddenRecaptcha-error').hide();
    //   $('#registerUserSubmit').prop("disabled", false);
// }  


$('#Email').change(function(e){
        e.preventDefault()
        var email = $(this).val();
        $.ajax({
            url: '/user/checkMail/' + email,
            method: 'POST',          
            dataType: 'json',
            data: email,
        }).done(function(data) {
            if(data.success == false){
                swal({title: "Мейлът е зает", type: "error"});
                $("#Email").val("Емайлът е зает");
                $("#Email" ).addClass( "form-control error" );
            }
        })
    });

$('#registerUserSubmit').on('click', function(e) {
        e.preventDefault()
        setTimeout(function() {
        if(($('#registerUser').valid() == true) && ($('#Email').valid() == true)) {
        
        var data_form = $('#registerUser').serialize();
        
        $.ajax({
            url: '/user/registerNewUser',
            method: 'POST',          
            dataType: 'json',
            data: data_form,
        })
            .done(function(data) {
                if(data.success == true){
                    swal({title: "Успешно се регистрирахте", type: "success"});
                   setTimeout(function () {
                    window.location.href = "/user/login";
                                }, 3000);
                    
                }  
            }).fail(function() {
                alert('<?php echo lang("please_fill"); ?>');
            });
        }
    },200);
});

</script>






<?php $this->load->view('templates/footer');  ?>
